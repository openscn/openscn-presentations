# Presentations

At this repository you can put any presentation file yout want to save.

To create a presentation create a directory named YYY-MM-DD for your presentation.

Inside that directory add your files using any format.

Optionally you can add a README.md at your presentation to tell more about it.

If you can recognize a pattern or want to propose one, please inform the rest of
the team.
