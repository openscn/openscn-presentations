/*
authors:
  ltasi@it.teithe.gr
  dpliakos@it.teithe.gr
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>

#define trigPin 14
#define echoPin 12
#define LED 2
#define THRESHOLD 10
#define  BUTTON 5

#define LOG_LEVEL_SILENT 1
#define LOG_LEVEL_PLOT 2
#define LOG_LEVEL_NORMAL 3
#define LOG_LEVEL_DEBUG 4

#define LOG_LEVEL LOG_LEVEL_DEBUG

#define SEND_ALERT true
#define TARGET_URL "################ PUT YOUR TARGET URL HERE ################"


// Add your access point cred here:
#ifndef STASSID
#define STASSID "################ PUT YOUR SSID HERE ################"
#define STAPSK  "################ PUT YOUR PASSWORD ################"
#endif


const char* ssid = STASSID;
const char* password = STAPSK;
bool ledTest = false;
bool shouldStart = false;
float duration, distance;




void setup() {
  pinMode(LED, OUTPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(BUTTON, INPUT);
  digitalWrite(LED, HIGH);

  Serial.begin(115200);

  if (LOG_LEVEL >= LOG_LEVEL_NORMAL) {
    Serial.println();
    Serial.print("connecting to ");
    Serial.println(ssid);
  }

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    blink();
  }

  if (LOG_LEVEL >= LOG_LEVEL_NORMAL) {
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }

  digitalWrite(LED, LOW);
}

void  blink() {
   ledTest = !ledTest;
    if (ledTest) {
      digitalWrite(LED, HIGH);
    } else {
      digitalWrite(LED, LOW);
    }
}

float findDistance() {
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);

    // Measure the response from the HC-SR04 Echo Pin

    duration = pulseIn(echoPin, HIGH);

    // Determine distance from duration
    // Use 343 metres per second as speed of sound
    float distance = (duration / 2) * 0.0343;
    return distance;
}

void sendAlert() {
  HTTPClient http;

  http.begin(TARGET_URL);     //Specify request destination

  int httpCode = http.GET();            //Send the request
  String payload = http.getString();    //Get the response payload

  if (LOG_LEVEL >= LOG_LEVEL_NORMAL)  {
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload
  }

  http.end();  //Close connection
}

void waitSomeTime() {
  int start = millis();
  int timeDelta = millis() - start;
  while(timeDelta < 10000) {
    blink();
    timeDelta = millis() - start;
    delay(200);
  }
}

void loop() {

  if (shouldStart) {
    // Write a pulse to the HC-SR04 Trigger Pin
    distance = findDistance();


    if (distance >= 400 || distance <= 2) {
      // Send results to Serial Monitor
      if (LOG_LEVEL >= LOG_LEVEL_DEBUG) {
        Serial.println("Out of range");
      }
    }
    else {
       if (distance > THRESHOLD) {
         digitalWrite(LED, LOW);

         if (SEND_ALERT) {
          sendAlert();
          waitSomeTime();
         }
      } else {
         digitalWrite(LED, HIGH);
      }

      if (LOG_LEVEL >= LOG_LEVEL_PLOT) {
        Serial.println(distance);
      }

      delay(10);
    }
  } else {
    int button = digitalRead(BUTTON);

    if (button == HIGH) {
      shouldStart = true;
    }

    blink();
    delay(60);
  }

  int button = digitalRead(BUTTON);
    if (LOG_LEVEL >= LOG_LEVEL_DEBUG) {
    Serial.print("Button: ");
    Serial.println(button);
  }

  delay(20);
}
