<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/* Exception class. */
require './Exception.php';

/* The main PHPMailer class. */
require './PHPMailer.php';

/* SMTP class, needed if you want to use SMTP. */
require './SMTP.php';

$config = parse_ini_file('../../../../../config.ini');
$uname = $config['username'];
$pwd = $config['password'];
$auth = $config['auth'];

$email_from = $config['email_from'];
$sender_username = $config['sender_username'];

$email_to = $config['email_to'];
$receiver_username = $config['receiver_username'];


$req = '??';

if(isset($_REQUEST['auth'])){
   $req = $_REQUEST['auth'];
}

if($auth == $req){ //if request is sent from an authenticated esp8266 then send email

   //log when ESP sent the request
   $logfile = 'door_opened.log';
   $when = date('d/m/Y H:i:s', time())."\r\n";
   file_put_contents($logfile, $when, FILE_APPEND);

   $mail = new PHPMailer(TRUE);

try {

   $mail->setFrom($email_from, $sender_username);
   $mail->addAddress($email_to, $receiver_username);
   $mail->Subject = 'Your door is open!';
   $mail->Body = 'Hey, There is a great disturbance in the Force.';

   /* SMTP parameters. */

   /* Tells PHPMailer to use SMTP. */
   $mail->isSMTP();

   /* SMTP server address. */
   $mail->Host = 'smtp.gmail.com';

   /* Use SMTP authentication. */
   $mail->SMTPAuth = TRUE;

   /* Set the encryption system. */
   $mail->SMTPSecure = 'tls';
   $mail->SMTPDebug = 0;
   /* SMTP authentication username. */
   $mail->Username = $uname;

   /* SMTP authentication password. */
   $mail->Password = $pwd;

   /* Set the SMTP port. */
   $mail->Port = 587;

   /* Finally send the mail. */
   if($mail->send()){
	  print 'mail sent';
   }
}
catch (Exception $e)
{
   echo $e->errorMessage();
}
catch (\Exception $e)
{
   echo $e->getMessage();
}
}//endif

?>
