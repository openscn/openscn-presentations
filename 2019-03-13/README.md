## Introduction to home automation using ESP8266

- Type: presentation
- Scope: technical
- Presentation slides: [https://docs.google.com/presentation/d/1m_fLOD5IULVaxzXPre0fp-B-G1tmbLXmmidQmjSSUXQ/edit?usp=sharing](https://docs.google.com/presentation/d/1m_fLOD5IULVaxzXPre0fp-B-G1tmbLXmmidQmjSSUXQ/edit?usp=sharing)
- Duration: 1h
- Needs hardware: yes
- Language: Greek
- Presenter(s):
  - ltasi (ltasi@it.teithe.gr)
  - dpliakos (dpliakos@it.teithe.gr)


  Την Τετάρτη 13/3 στις 12:30 στην αίθουσα 109 θα γίνει παρουσίαση από τους φοιτητές Δημήτρη Πλιάκο
  και Λοράν Τάσι στα πλαίσια του πρότζεκτ "Smart Campus" που υλοποιείται στο ΤΕΙ με θέμα "Εισαγωγή στο Home Automation με esp8266".

  Το πρόβλημα που θα λύσουμε είναι η απομακρυσμένη παρακολούθηση χώρου και συγκεκριμένα της πόρτας και σε περίπτωση που ανοίξει να λάβουμε ειδοποίηση. Ο στόχος είναι να προβληθεί η ευκολία υλοποίησης ενός παρόμοιου πρότζεκτ και από αρχάριους μέσω του esp8266.

  Τι θα αποκομίσετε από την παρουσίαση:

    Θα μάθετε τι είναι το esp8266,
    Πόσο εύκολο είναι να κάνετε κάτι μόνοι σας
    Πως το υλοποιήσαμε
    Έμπνευση και ιδέες!

  Η παρακολούθηση δεν απαιτεί προηγούμενη εμπειρία.
  Σας περιμένουμε
